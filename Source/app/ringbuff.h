#ifndef _RINGBUFF_H_
#define _RINGBUFF_H_
/** 环形缓冲区数据结构 */
typedef struct {
    size_t rbCapacity;
    uint8_t  *rbHead;
    uint8_t  *rbTail;
    uint8_t  *rbBuff;
}rb_t;


int32_t rbCanRead(rb_t *rb);

int32_t rbWrite(rb_t *rb, const void *data, size_t count);
int32_t rbRead(rb_t *rb, void *data, size_t count);
void gizwitsInit(void);

#endif
