/**
  ******************************************************************************
  * @file    main.c
  * @author  cheng bb
  * @version V2.0
  * @date    2016-10-04
  * @brief   main头文件
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2016 MindX</center></h2>
  ******************************************************************************
  */
#include "stm32f10x.h"
#include "relay.h"
#include "systick.h"
#include "uart1.h"
#include "logic_control.h"
#include "pwm_out.h"
#include "adc.h"
#include "adc_value.h"
#include "uart2.h"
#include "timer.h"
#include "ringbuff.h"

extern rb_t pRb;
/***********************************************************
 * @Function    : main
 * @Description : 主程序运行
 * @Input       : None
 * @Output      : None
 * @Return      : None
 ***********************************************************/	
int main(void)
{
	uint8_t count = 0;
	uint32_t i;
	uint8_t WriteBuff[10] = {0,1,2,3,4,5,6,7,8,9};
	uint8_t ReadBuff[10];
	SysTick_Init();
	UART1_Init();
	TIM4_Init();
	UART2_Init();
	Relay_Init();
	TIM3_Init();
	ADC1_Init();
	printf("cbb.c\r\n");	
	gizwitsInit();

  	while(1)
	{		
		for(i=0;i<0xffffff;i++);
//		count = rbWrite(&pRb, WriteBuff, 8);

		count = rbCanRead(&pRb);
		rbRead(&pRb, ReadBuff, count);
		printf("ring buffer can read %d size\n",count);
		for(i=0;i<count;i++)
		{
			printf("%c ",ReadBuff[i]);
		}
		printf("\r\n");
//		Test_Control();
		
	
    }				
}

/*********************file end (c)MindX************************/

