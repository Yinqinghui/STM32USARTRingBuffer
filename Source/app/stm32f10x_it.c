/**
  ******************************************************************************
  * @file    stm32f10x_it.c
  * @author  cheng bb
  * @version V2.0
  * @date    2016-10-04
  * @brief   中断配置头文件
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2016 CBB</center></h2>
  ******************************************************************************
  */
#include "stm32f10x_it.h"
#include "stdio.h"
#include "string.h"
#include "systick.h"
#include "adc.h"
#include "logic_control.h"
#include "pwm_out.h"

char Recv_Data_PC_BUFF[256];
char Recv_Data_PC[256];

uint8_t  Recv_Data_WT_BUFF[13];
uint8_t  Recv_Data_WT[13];
uint8_t  i = 0;
uint8_t  j = 0;
uint8_t  Recv_Leak = 0;
uint16_t Recv_TDS = 0;
uint32_t Recv_Flow = 0;


/***********************************************************
 * @Function    : SysTick_Handler
 * @Description : 嘀嗒定时器中断函数
 * @Input       : None
 * @Output      : None
 * @Return      : None
 ***********************************************************/
void SysTick_Handler(void)
{
	TimingDelay_Decrement();	
}



/***********************************************************
 * @Function    : USART2_IRQHandler
 * @Description : UART2 中断函数 
 * @Input       : None
 * @Output      : None
 * @Return      : None
 ***********************************************************/
void USART2_IRQHandler(void)
{	
	if(USART_GetITStatus(USART2, USART_IT_RXNE) != RESET)
	{
		Recv_Data_WT_BUFF[j] = (uint8_t)USART_ReceiveData(USART2);
 
        j = j + 1;	
		
		if((Recv_Data_WT_BUFF[0] == 0xAA) && (Recv_Data_WT_BUFF[1] == 0x55) && 
		(Recv_Data_WT_BUFF[11] == 0xCC) && (Recv_Data_WT_BUFF[12] == 0x33))
		{   
			j = 0;
			memcpy(Recv_Data_WT, Recv_Data_WT_BUFF, 13);
			Recv_TDS = Recv_Data_WT[2] << 8 | Recv_Data_WT[3];
			Recv_Leak = Recv_Data_WT[8];
			Recv_Flow = Recv_Data_WT[4] << 24 | Recv_Data_WT[5] << 16 | Recv_Data_WT[6] << 8 | Recv_Data_WT[7];
			
//			printf("Recv_TDS = %d\r\n",Recv_TDS);
//			printf("Recv_Leak = %d\r\n",Recv_Leak);
			printf("Recv_Flow = %d\r\n",Recv_Flow);
			memset(Recv_Data_WT_BUFF, 0, 13);
			memset(Recv_Data_WT, 0, 13);
		}
		
		if(j == 14)
		{
		    j = 0;
			memset(Recv_Data_WT_BUFF, 0, 13);
			memset(Recv_Data_WT, 0, 13);
		}		
	}			
}

/***********************************************************
 * @Function    : DMA1_Channel1_IRQHandler
 * @Description : DMA1串口中断函数 
 * @Input       : None
 * @Output      : None
 * @Return      : None
 ***********************************************************/
void DMA1_Channel1_IRQHandler(void) 
{
	if(DMA_GetITStatus(DMA1_IT_TC1) != RESET)
	{
	    Flag_ADC = 1;
		DMA_ClearITPendingBit(DMA1_IT_TC1);
	}
}

/***********************************************************
 * @Function    : TIM4_IRQHandler
 * @Description : TIM4 中断函数 
 * @Input       : None
 * @Output      : None
 * @Return      : None
 ***********************************************************/
void TIM4_IRQHandler(void)
{	  
	if(TIM_GetITStatus(TIM4 , TIM_IT_Update) != RESET)
	{
		printf("cbb.c.........\r\n");
		Flow_Flag = 1;
		TIM_ClearITPendingBit(TIM4 , TIM_IT_Update);  
	}
}
/*********************file end (c)CBB************************/
