/**
  ******************************************************************************
  * @file    systick.h
  * @author  cheng bb
  * @version V2.0
  * @date    2016-10-04
  * @brief   systick����ͷ�ļ�
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2016 CBB</center></h2>
  ******************************************************************************
  */
#ifndef __SYSTICK_H_
#define __SYSTICK_H_

#include "stm32f10x.h"

void TimingDelay_Decrement(void);
void SysTick_Init(void);
void Delay_us(__IO u32 nTime);	 

#define Delay_ms(x) Delay_us(1000*x)	 

#endif 

/*********************file end (c)CBB************************/
