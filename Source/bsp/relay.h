/**
  ******************************************************************************
  * @file    realy.h
  * @author  cheng bb
  * @version V2.0
  * @date    2016-10-19
  * @brief   �̵�������ͷ�ļ�
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2016 MindX</center></h2>
  ******************************************************************************
  */

#ifndef __RELAY_H_
#define __RELAY_H_

#define  RELAY_PIN_LEAK_SWITCH     GPIO_Pin_0    
#define  RELAY_PIN_POWER_SWITCH    GPIO_Pin_8
#define  RELAY_PIN_TDS_SWITCH      GPIO_Pin_9

#define  RELAY_PORT                GPIOB
#define  READY_RCC                 RCC_APB2Periph_GPIOB

void Relay_Init(void);
	
#endif

/*********************file end (c)MindX************************/
