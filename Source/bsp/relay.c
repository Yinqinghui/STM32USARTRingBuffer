/**
  ******************************************************************************
  * @file    relay.c
  * @author  cheng bb
  * @version V2.0
  * @date    2016-10-18
  * @brief   �̵�������ͷ�ļ�
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2016 MindX</center></h2>
  ******************************************************************************
  */

#include "systick.h"
#include "relay.h"

/***********************************************************
 * @Function    : Relay_GPIO_Config
 * @Description : �̵��� GPIO����
 * @Input       : None
 * @Output      : None
 * @Return      : None
 ***********************************************************/	
static void Relay_GPIO_Config(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;
	RCC_APB2PeriphClockCmd(READY_RCC, ENABLE);
     
	GPIO_InitStructure.GPIO_Pin = RELAY_PIN_LEAK_SWITCH | RELAY_PIN_POWER_SWITCH | RELAY_PIN_TDS_SWITCH;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;  
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	  
	GPIO_Init(RELAY_PORT, &GPIO_InitStructure);	

    GPIO_ResetBits(RELAY_PORT, RELAY_PIN_LEAK_SWITCH);
    GPIO_ResetBits(RELAY_PORT, RELAY_PIN_POWER_SWITCH);
	GPIO_ResetBits(RELAY_PORT, RELAY_PIN_TDS_SWITCH);  
}

void Relay_Init(void)
{
	Relay_GPIO_Config();
}

/*********************file end (c)MindX************************/

