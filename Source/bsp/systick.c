/**
  ******************************************************************************
  * @file    systick.c
  * @author  cheng bb
  * @version V2.0
  * @date    2016-10-04
  * @brief   systick配置头文件
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2016 CBB</center></h2>
  ******************************************************************************
  */
	
#include "systick.h"

uint32_t TimingDelay;

#define  ONE_MICROSECOND  1000000            //1us
#define  TEN_MICROSECOND  100000             //10us
#define  ONE_MILLISECOND  1000               //1ms


/***********************************************************
 * @Function    : SysTick_Init
 * @Description : 启动系统滴答定时器 SysTick
 * @Input       : None
 * @Output      : None
 * @Return      : None
 ***********************************************************/		
void SysTick_Init(void)
{

	if (SysTick_Config(SystemCoreClock / ONE_MICROSECOND))	  // ST3.5.0库版本
	{ 
		while (1);
	}
	
	SysTick->CTRL &= ~ SysTick_CTRL_ENABLE_Msk;               // 关闭滴答定时器  
}


/***********************************************************
 * @Function    : Delay_us
 * @Description : us延时程序,10us为一个单位
 * @Input       : __IO u32 nTime
 * @Output      : None
 * @Return      : None
 ***********************************************************/		
void Delay_us(__IO u32 nTime)
{ 
	TimingDelay = nTime;	

	SysTick->CTRL |=  SysTick_CTRL_ENABLE_Msk;    // 使能滴答定时器  

	while(TimingDelay != 0);
}


/***********************************************************
 * @Function    : TimingDelay_Decrement
 * @Description : 获取节拍程序
 * @Input       : None
 * @Output      : None
 * @Return      : None
 ***********************************************************/		
void TimingDelay_Decrement(void)
{
	if(TimingDelay != 0x00)
	{ 
		TimingDelay--;
	}
}

/*********************file end (c)CBB************************/
