/**
  ******************************************************************************
  * @file    timer.c
  * @author  cheng bb
  * @version V2.0
  * @date    2016-10-28
  * @brief   定时器头文件
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2016 MindX</center></h2>
  ******************************************************************************
  */
#include "timer.h"

#define PRES_VALUE     7199
#define PERIOD_VALUE   9999

/***********************************************************
 * @Function    : TIM4_Config
 * @Description : TIM4配置
 * @Input       : None
 * @Output      : None
 * @Return      : None
 ***********************************************************/	
static void TIM4_Config(void)
{
	/**** TIMx_CNT   脉冲计数器*************count*********************/  
	/**** TIMx_CCR   捕获/比较寄存器capture compare register**********/  

	TIM_TimeBaseInitTypeDef   TIM_TimeBaseStructure;
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);

	TIM_DeInit(TIM4);

	TIM_TimeBaseStructure.TIM_Period = 7199;             //计数值，当计数器记到这个值时会溢出产生中断或DMA请求  TIMx_ARR重装载寄存器
	TIM_TimeBaseStructure.TIM_Prescaler = 9999;            //时钟预分屏   PSC
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;     			 //时钟分频系数
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;  //向上计数模式
	TIM_TimeBaseStructure.TIM_RepetitionCounter=0;               //重复计数，重复计数多少次才来一次溢出中断  TIMx_RCR重复次数寄存器
	TIM_TimeBaseInit(TIM4, &TIM_TimeBaseStructure);

	TIM_ClearFlag(TIM4, TIM_FLAG_Update);						 //清除
	TIM_ITConfig(TIM4,TIM_IT_Update, DISABLE);                    //中断控制开启
	
	TIM_Cmd(TIM4, DISABLE);    
}

/***********************************************************
 * @Function    : TIM4_NVIC_Config
 * @Description : TIM4中断配置
 * @Input       : None
 * @Output      : None
 * @Return      : None
 ***********************************************************/	
static void TIM4_NVIC_Config(void)
{
	NVIC_InitTypeDef NVIC_InitStructure;                         //中断控制寄存器配置
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);              //对NVIC中断优先级分组设置  
	
	NVIC_InitStructure.NVIC_IRQChannel = TIM4_IRQn;              //中断向量配置
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;    //配置相应中断向量的抢占优先级
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;           //配置相应中断向量的响应优先级	
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;              //使能或关闭相应中断向量的优先级
	
	NVIC_Init(&NVIC_InitStructure);
}


/***********************************************************
 * @Function    : TIM4_Stop
 * @Description : TIM4停止
 * @Input       : None
 * @Output      : None
 * @Return      : None
 ***********************************************************/	
void TIM4_Stop(void)
{
	TIM_ITConfig(TIM4,TIM_IT_Update, DISABLE);                     //中断控制开启
	TIM_Cmd(TIM4, DISABLE);                                        //开启定时器				
}

/***********************************************************
 * @Function    : TIM4_Restart
 * @Description : TIM4启动
 * @Input       : None
 * @Output      : None
 * @Return      : None
 ***********************************************************/	
void TIM4_Restart(void)
{
    TIM_ITConfig(TIM4,TIM_IT_Update, ENABLE);                     //中断控制开启
	TIM_Cmd(TIM4, ENABLE);                                        //开启定时器	
}

/***********************************************************
 * @Function    : TIM4_Init
 * @Description : TIM4定时器初始化
 * @Input       : None
 * @Output      : None
 * @Return      : None
 ***********************************************************/	
void TIM4_Init(void)
{
	TIM4_Config();	
	TIM4_NVIC_Config();
}

/*********************file end (c)MindX************************/

