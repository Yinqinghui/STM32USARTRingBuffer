/**
  ******************************************************************************
  * @file    logic_control.c
  * @author  cheng bb
  * @version V2.0
  * @date    2016-10-18
  * @brief   逻辑执行头文件
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2016 MindX</center></h2>
  ******************************************************************************
  */
#include "stdio.h"
#include "string.h"
#include "stdlib.h"
#include "logic_control.h"
#include "relay.h"
#include "uart1.h"
#include "pwm_out.h"
#include "adc_value.h"
#include "adc.h"
#include "timer.h"

uint8_t AD_Loop = 0;
uint8_t Flow_Flag = 0;

/***********************************************************
 * @Function    : Power_Relay_Control
 * @Description : 电源继电器控制
 * @Input       : None
 * @Output      : None
 * @Return      : None
 ***********************************************************/	
static void Power_Relay_Control(void)
{

	if( strcmp(Recv_Data_PC,POWER_RELAY_OPEN_COMMAND) == 0)
	{
//		printf("power success\r\n");
	    GPIO_SetBits(RELAY_PORT, RELAY_PIN_POWER_SWITCH);
	}
}

/***********************************************************
 * @Function    : TDS_Relay_Control
 * @Description : TDS继电器控制
 * @Input       : None
 * @Output      : None
 * @Return      : None
 ***********************************************************/
static void TDS_Relay_Control(void)
{
    if(strcmp(Recv_Data_PC,TDS_RELAY_OPEN_COMMAND) == 0)
	{
		GPIO_SetBits(RELAY_PORT, RELAY_PIN_TDS_SWITCH);
	
		if(Recv_TDS > 900 && Recv_TDS < 950)                  //921   923
		{
		    printf("TDS Test Success, Pass\r\n");
			memset(Recv_Data_PC, 0, sizeof(Recv_Data_PC));
			GPIO_ResetBits(RELAY_PORT, RELAY_PIN_TDS_SWITCH);
		}
		else
		{
			printf("TDS Test Fail\r\n");
			memset(Recv_Data_PC, 0, sizeof(Recv_Data_PC));
		}
	}
}

/***********************************************************
 * @Function    : Leak_Relay_Control
 * @Description : 漏水继电器控制
 * @Input       : None
 * @Output      : None
 * @Return      : None
 ***********************************************************/
static void Leak_Relay_Control(void)
{
	if(strcmp(Recv_Data_PC,LEAK_RELAY_OPEN_COMMAND) == 0)
	{
		GPIO_SetBits(RELAY_PORT, RELAY_PIN_LEAK_SWITCH);
		if(Recv_Leak == 1)     					 // <= 3V
		{
		    printf("Leak Test Success, Pass\r\n");
			memset(Recv_Data_PC, 0, sizeof(Recv_Data_PC));
			GPIO_SetBits(RELAY_PORT, RELAY_PIN_LEAK_SWITCH);
		}
		else                    				// > 3  <=3.3
		{
			printf("Leak Test Fail\r\n");
			memset(Recv_Data_PC, 0, sizeof(Recv_Data_PC));
		}
	}
}

/***********************************************************
 * @Function    : PWM_Out_Control
 * @Description : PWM输出流量检测控制
 * @Input       : None
 * @Output      : None
 * @Return      : None
 ***********************************************************/
static void PWM_Out_Flow_Control(void)
{
    if(strcmp(Recv_Data_PC,PWM_PUTOUT_COMMAND) == 0)
	{
		TIM4_Restart();
		PWM_Restart();
		while(Flow_Flag == 1)
		{
			TIM4_Stop();
			PWM_Trans_Stop();
			printf("Recv_Flow111 = %d\r\n",Recv_Flow);
			
			if(Recv_Flow > 400)
			{
				printf("Flow Test Success ,PASS\r\n");
				Flow_Flag = 0;
				memset(Recv_Data_PC, 0, sizeof(Recv_Data_PC));
				TIM4_Stop();
				PWM_Trans_Stop();
			}
			else
			{
				printf("Flow Test Fail\r\n");
				Flow_Flag = 0; 
				memset(Recv_Data_PC, 0, sizeof(Recv_Data_PC));
				TIM4_Stop();
				PWM_Trans_Stop();
			}
		}
	}
}

/***********************************************************
 * @Function    : ADC_Three_Five_Control
 * @Description : 电源检测控制
 * @Input       : None
 * @Output      : None
 * @Return      : None
 ***********************************************************/
static void ADC_Three_Five_Control(void)
{
    if(strcmp(Recv_Data_PC,ADC_THREE_FIVE_COMMEND) == 0)
	{
		if(AD_Loop < 30) 										//AD采集30次，使采集数据更稳定
		{    
		     ADC_Process();
			 AD_Loop = AD_Loop + 1;
			 return; 
		}
		/**********5V test***********/
		if((Voltage.AD_Value_Five) > 2.4 && (Voltage.AD_Value_Five <= 2.5))  						//水盒子5V电源合格2.43
		{
			printf("5V Power Test Success ,PASS\r\n");
            memset(Recv_Data_PC, 0, sizeof(Recv_Data_PC));
			AD_Loop = 0;			
		}		
		else 													//水盒子5V电源测试不合格
		{
		    printf("5V Power Test Fail\r\n");
			memset(Recv_Data_PC, 0, sizeof(Recv_Data_PC));
			AD_Loop = 0;
		}
		
		/**********3.3 test***********/
		if((Voltage.AD_Value_Three > 2.6) && (Voltage.AD_Value_Three <= 3.3))  						//水盒子3.3V电源合格2.73
		{
			printf("3.3V Power Test Success ,PASS\r\n");
            memset(Recv_Data_PC, 0, sizeof(Recv_Data_PC));
			AD_Loop = 0;			
		}	
		else 													//水盒子3.3V电源测试不合格
		{
		    printf("3.3V Power Test Fail\r\n");
			memset(Recv_Data_PC, 0, sizeof(Recv_Data_PC));
			AD_Loop = 0;
		}
	}
		
}

/***********************************************************
 * @Function    : Test_Control
 * @Description : 测试逻辑函数 
 * @Input       : None
 * @Output      : None
 * @Return      : None
 ***********************************************************/
void Test_Control(void)
{
	Power_Relay_Control();
	TDS_Relay_Control();
	Leak_Relay_Control();
	PWM_Out_Flow_Control();
	ADC_Three_Five_Control();
}

/*********************file end (c)MindX************************/


