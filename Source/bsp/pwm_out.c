/**
  ******************************************************************************
  * @file    pwm_out.c
  * @author  cheng bb
  * @version V2.0
  * @date    2016-10-20
  * @brief   PWM输出头文件
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2016 MindX</center></h2>
  ******************************************************************************
  */
#include "pwm_out.h"


#define PRES_VALUE     	   0
#define PERIOD_VALUE       1199
#define CCR1_Val           500

/***********************************************************
 * @Function    : TIM3_PWM_Out_GPIO_Config
 * @Description : TIM3 PWM输出管脚配置
 * @Input       : None
 * @Output      : None
 * @Return      : None
 ***********************************************************/	
static void TIM3_PWM_Out_GPIO_Config(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;
	RCC_APB2PeriphClockCmd(PWM_OUT_RCC | RCC_APB2Periph_AFIO, ENABLE);
	
	GPIO_PinRemapConfig(GPIO_FullRemap_TIM3, ENABLE);

	GPIO_InitStructure.GPIO_Pin = PWM_OUT_GPIO_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;

	GPIO_Init(PWM_OUT_PORT, &GPIO_InitStructure);
}

/***********************************************************
 * @Function    : TIM3_Config
 * @Description : TIM2配置
 * @Input       : None
 * @Output      : None
 * @Return      : None
 ***********************************************************/	
static void TIM3_Config(void)
{
	/**** TIMx_CNT   脉冲计数器*************count*********************/  
	/**** TIMx_CCR   捕获/比较寄存器capture compare register**********/  

	TIM_TimeBaseInitTypeDef   TIM_TimeBaseStructure;
	TIM_OCInitTypeDef         TIM_OCInitStructure;

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);	
	TIM_DeInit(TIM3);

	TIM_TimeBaseStructure.TIM_Period = 1199;             //计数值，当计数器记到这个值时会溢出产生中断或DMA请求  TIMx_ARR重装载寄存器
	TIM_TimeBaseStructure.TIM_Prescaler = 0;            //时钟预分屏   PSC
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;      //时钟分频系数
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;  //向上计数模式
	TIM_TimeBaseStructure.TIM_RepetitionCounter=0;               //重复计数，重复计数多少次才来一次溢出中断  TIMx_RCR重复次数寄存器

	TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);

	/* PWM1 Mode configuration: Channel1 */
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;               //定时器输出模式配置，一般为TIM_OCMode_PWM1或TIM_OCMode_PWM2
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;   //定时器输出使能
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;       //有效电平极性
	TIM_OCInitStructure.TIM_Pulse = CCR1_Val;                       //捕获/比较寄存器值
	TIM_OC1Init(TIM3, &TIM_OCInitStructure); 
	TIM_OC1PreloadConfig(TIM3, TIM_OCPreload_Enable);

	/* 使能TIM3重载寄存器ARR*/
	TIM_ARRPreloadConfig(TIM3, DISABLE);	

	TIM_Cmd(TIM3, DISABLE);                                          //开启定时器		
//	TIM_Cmd(TIM3, ENABLE); 
}

/***********************************************************
 * @Function    : PWM_Trans_Stop
 * @Description : PWM脉冲输出停止
 * @Input       : None
 * @Output      : None
 * @Return      : None
 ***********************************************************/
void PWM_Trans_Stop(void)
{
	TIM_ARRPreloadConfig(TIM3, DISABLE);	
	TIM_Cmd(TIM3, DISABLE); 
}

/***********************************************************
 * @Function    : PWM_Restart
 * @Description : PWM脉冲传输
 * @Input       : None
 * @Output      : None
 * @Return      : None
 ***********************************************************/
void PWM_Restart(void)
{
	TIM_ARRPreloadConfig(TIM3, ENABLE);
    TIM_Cmd(TIM3, ENABLE); 
}

/***********************************************************
 * @Function    : TIM3_Init
 * @Description : TIM3定时器初始化
 * @Input       : None
 * @Output      : None
 * @Return      : None
 ***********************************************************/	
void TIM3_Init(void)
{
	TIM3_PWM_Out_GPIO_Config();
	TIM3_Config();  
}

/*********************file end (c)MindX************************/

