/**
  ******************************************************************************
  * @file    adc_value.c
  * @author  cheng bb
  * @version V2.0
  * @date    2016-10-21
  * @brief   ADC采集头文件
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2016 MindX</center></h2>
  ******************************************************************************
  */
#include "adc_value.h"
#include "adc.h"
#include "uart1.h"
#include "systick.h"

VOLTAGE Voltage;

/*******************************************************************************
 * @Function    : Get_ADC_Power_Five_Value
 * @Description : 获取5V电源电压值
 * @Input       : None
 * @Output      : None
 * @Return      : None
 *******************************************************************************/
static float Get_ADC_Power_Five_Value(void)  //PC0    水盒子	5V电源测试
{
	int i = 0;
    uint32_t ADC_Mean_Value = 0;
	float Aver_Five = 0;
	
    for(i = 0;i < SAMP_COUNT; i++)
	{
		ADC_Mean_Value +=  AD_Value[i][0];
	}
	Aver_Five = ADC_Mean_Value / SAMP_COUNT;
	
	return Aver_Five;
}

/*******************************************************************************
 * @Function    : Get_ADC_Power_Five_Value
 * @Description : 获取3.3V电源电压值
 * @Input       : None
 * @Output      : None
 * @Return      : None
 *******************************************************************************/
static float Get_ADC_Power_Three_Value(void)//PC1    水盒子      3.3V电源测试
{
	int i = 0;
    uint32_t  ADC_Mean_Value = 0;
	float Aver_Three = 0;
	
    for(i = 0;i < SAMP_COUNT; i++)
	{
		ADC_Mean_Value +=  AD_Value[i][1];
	}
	
	Aver_Three = ADC_Mean_Value / SAMP_COUNT;
	
	return Aver_Three;
}

/*******************************************************************************
 * @Function    : Get_Voltage
 * @Description : 获取电压值
 * @Input       : None
 * @Output      : None
 * @Return      : None
 *******************************************************************************/
static void Get_Voltage(void)
{
	
	Voltage.AD_Value_Five = Get_ADC_Power_Five_Value() / 4096 * 3.3;      //5V电压测试
	Voltage.AD_Value_Three = Get_ADC_Power_Three_Value() / 4096 * 3.3;    //3.3V电压测试
	
//	printf("AD_Five_AD = %.0f\r\n",Get_ADC_Power_Five_Value());
//	printf("AD_Value_Five = %.2f V\r\n",Voltage.AD_Value_Five);
//	printf("ADC_Three_AD = %.0f\r\n",Get_ADC_Power_Three_Value());
	printf("AD_Value_Three = %.2f V\r\n",Voltage.AD_Value_Three);
//	printf("\r\n");
	
	Delay_us(500000);
}

/*******************************************************************************
 * @Function    : ADC_Process
 * @Description : 电压采集过程
 * @Input       : None
 * @Output      : None
 * @Return      : None
 *******************************************************************************/
void ADC_Process(void)
{
    if(1 == Flag_ADC)
	{
		Flag_ADC = 0;
//		printf("hdjkghdjkgd\r\n");
		ADC1_Trans_Stop();
		Get_Voltage();
		ADC1_Restart();
		
		

	}
}
/*********************file end (c)MindX************************/

