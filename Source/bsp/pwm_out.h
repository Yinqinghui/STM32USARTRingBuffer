/**
  ******************************************************************************
  * @file    pwm_out.h
  * @author  cheng bb
  * @version V2.0
  * @date    2016-10-20
  * @brief   PWM���ͷ�ļ�
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2016 MindX</center></h2>
  ******************************************************************************
  */
#ifndef __PWM_OUT_H_
#define __PWM_OUT_H_
#include "stm32f10x.h"
#include "stdio.h"

#define PWM_OUT_RCC        RCC_APB2Periph_GPIOC
#define PWM_OUT_RCC_AFIO   RCC_APB2Periph_AFIO
#define PWM_OUT_GPIO_PIN   GPIO_Pin_6
#define PWM_OUT_PORT       GPIOC



void PWM_Trans_Stop(void);
void PWM_Restart(void);
void TIM3_Init(void);
 
#endif

/*********************file end (c)MindX************************/
