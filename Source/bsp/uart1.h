/**
  ******************************************************************************
  * @file    uart1.h
  * @author  cheng bb
  * @version V2.0
  * @date    2016-10-09
  * @brief   uart1ͷ�ļ�
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2016 CBB</center></h2>
  ******************************************************************************
  */
#ifndef __USART1_H_
#define	__USART1_H_

#include "stm32f10x.h"
#include <stdio.h>

void UART1_Init(void);

#endif 

/*********************file end (c)CBB************************/
