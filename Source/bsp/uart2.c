/**
  ******************************************************************************
  * @file    uart2.c
  * @author  cheng bb
  * @version V2.0
  * @date    2016-10-26
  * @brief   UART2头文件
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2016 MindX</center></h2>
  ******************************************************************************
  */
#include "uart2.h"
#include "uart1.h"


/***********************************************************
 * @Function    : UART2_GPIO_Config
 * @Description : UART2 TX RX管脚配置
 * @Input       : None
 * @Output      : None
 * @Return      : None
 ***********************************************************/	
 void UART2_GPIO_Config(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;

	GPIO_Init(GPIOA, &GPIO_InitStructure);
}

/***********************************************************
 * @Function    : UART2_NVIC_Config
 * @Description : UART2 中断配置
 * @Input       : None
 * @Output      : None
 * @Return      : None
 ***********************************************************/	
static void UART2_NVIC_Config(void)
{
	NVIC_InitTypeDef NVIC_InitStructure;

//	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);
	
	NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;                 //中断通道
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;         //抢占式优先级
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;                //子优先级
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;                   //中断通道使能

	NVIC_Init(&NVIC_InitStructure);
}

/***********************************************************
 * @Function    : USART2_Config
 * @Description : UART2配置
 * @Input       : None
 * @Output      : None
 * @Return      : None
 ***********************************************************/	
static void USART2_Config( uint32_t bound)
{
	USART_InitTypeDef USART_InitStructure;
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);

	USART_DeInit(USART2); //UART1复位

	USART_InitStructure.USART_BaudRate = bound;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No ;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

	USART_Init(USART2, &USART_InitStructure);

	USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);	
	USART_Cmd(USART2, ENABLE);
}

/***********************************************************
 * @Function    : UART2_Init
 * @Description : UART2初始化
 * @Input       : None
 * @Output      : None
 * @Return      : None
 ***********************************************************/	
void UART2_Init(void)
{
	UART2_GPIO_Config();
	UART2_NVIC_Config();
    USART2_Config(9600);
}

/*********************file end (c)MindX************************/

