/**
  ******************************************************************************
  * @file    uart1.c
  * @author  cheng bb
  * @version V2.0
  * @date    2016-10-09
  * @brief   uart1头文件
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2016 CBB</center></h2>
  ******************************************************************************
  */
#include "uart1.h"
#include "ringbuff.h"

extern rb_t pRb;
/***********************************************************
 * @Function    : UART1_GPIO_Config
 * @Description : UART1 TX RX管脚配置
 * @Input       : None
 * @Output      : None
 * @Return      : None
 ***********************************************************/	
 static void UART1_GPIO_Config(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;

	GPIO_Init(GPIOA, &GPIO_InitStructure);
}

/***********************************************************
 * @Function    : UART1_NVIC_Config
 * @Description : UART1 中断配置
 * @Input       : None
 * @Output      : None
 * @Return      : None
 ***********************************************************/	
static void UART1_NVIC_Config(void)
{
	NVIC_InitTypeDef NVIC_InitStructure;

	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;                 //中断通道
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;         //抢占式优先级
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;                //子优先级
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;                   //中断通道使能

	NVIC_Init(&NVIC_InitStructure);
}

/***********************************************************
 * @Function    : USART1_Config
 * @Description : UART1配置
 * @Input       : None
 * @Output      : None
 * @Return      : None
 ***********************************************************/	
static void USART1_Config( uint32_t bound)
{
	USART_InitTypeDef USART_InitStructure;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);

	USART_DeInit(USART1); //U ART1复位

	USART_InitStructure.USART_BaudRate = bound;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No ;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

	USART_Init(USART1, &USART_InitStructure);

	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);	
	USART_Cmd(USART1, ENABLE);
}

/***********************************************************
 * @Function    : fputc
 * @Description : 重定向c库函数printf到USART1
 * @Input       : int ch, FILE *f
 * @Output      : None
 * @Return      : ch
 ***********************************************************/	
int fputc(int ch, FILE *f)
{		
	USART_SendData(USART1, (uint8_t) ch);                            //发送一个字节数据到USART1
	while (USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET);	   //等待发送完毕 	
	return (ch);
}

/***********************************************************
 * @Function    : fgetc
 * @Description : 重定向c库函数scanf到USART1
 * @Input       : FILE *f
 * @Output      : None
 * @Return      : USART_ReceiveData(USART1)
 ***********************************************************/	
int fgetc(FILE *f)
{
	while (USART_GetFlagStatus(USART1, USART_FLAG_RXNE) == RESET);   //等待串口1输入数据
	return (int)USART_ReceiveData(USART1);
}

/***********************************************************
 * @Function    : UART1_Init
 * @Description : UART1初始化
 * @Input       : None
 * @Output      : None
 * @Return      : None
 ***********************************************************/	
void UART1_Init(void)
{
	UART1_GPIO_Config();
	UART1_NVIC_Config();
    USART1_Config(115200);
}

/**
*************************************************************
 * @Function    : USART1_IRQHandler
 * @Description : UART1 中断函数 
 * @Input       : None
 * @Output      : None
 * @Return      : None
 ***********************************************************/
void USART1_IRQHandler(void)
{	
	uint8_t tmp = 0;
	if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)
	{
		tmp = USART_ReceiveData(USART1);	
		rbWrite(&pRb,&tmp,1);
	}			
}
