/**
  ******************************************************************************
  * @file    adc_value.h
  * @author  cheng bb
  * @version V2.0
  * @date    2016-10-21
  * @brief   ADC采集头文件
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2016 MindX</center></h2>
  ******************************************************************************
  */
#include "stm32f10x.h"
 
typedef struct
{
    float AD_Value_Five;     //5V电压
    float AD_Value_Three;    //3.3V电压
}VOLTAGE;

extern VOLTAGE Voltage;
void ADC_Process(void);

/*********************file end (c)MindX************************/
