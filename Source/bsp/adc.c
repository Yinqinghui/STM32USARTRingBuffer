/**
  ******************************************************************************
  * @file    adc.c
  * @author  cheng bb
  * @version V2.0
  * @date    2016-10-20
  * @brief   ADC配置头文件
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2016 MindX</center></h2>
  ******************************************************************************
  */
#include "adc.h"

uint16_t AD_Value[SAMP_COUNT + 1][CIRCUIT];
uint8_t  Flag_ADC;

/***********************************************************
 * @Function    : ADC1_GPIO_Config
 * @Description : AD转换GPIO管脚配置
 * @Input       : None
 * @Output      : None
 * @Return      : None
 ***********************************************************/
static void ADC1_GPIO_Config(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;
	RCC_APB2PeriphClockCmd(ADC1_RCC | ADC1_RCC_AFIO, ENABLE);

	GPIO_InitStructure.GPIO_Pin = ADC1_GPIO_THREE_PIN | ADC1_GPIO_FIVE_PIN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;

	GPIO_Init(ADC1_GPIO_PORT, &GPIO_InitStructure);
}

/***********************************************************
 * @Function    : TIM2_Config
 * @Description : TIM2配置
 * @Input       : None
 * @Output      : None
 * @Return      : None
 ***********************************************************/
static void TIM2_Config(void)
{
    TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	TIM_OCInitTypeDef TIM_OCInitStructure;
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2 , ENABLE);
	
	TIM_DeInit(TIM2);
	
	TIM_TimeBaseStructure.TIM_Period = 999;
	TIM_TimeBaseStructure.TIM_Prescaler = 11;       
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;    
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;  
	TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure);
	
    TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM2;                 
    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;    
    TIM_OCInitStructure.TIM_Pulse = 500;                              
    TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;        
    TIM_OC2Init(TIM2, &TIM_OCInitStructure);                          
    TIM_OC2PreloadConfig(TIM2, TIM_OCPreload_Enable);     

	TIM_DMACmd(TIM2, TIM_DMA_Update, ENABLE);
	
    TIM_Cmd(TIM2, ENABLE);
}

/***********************************************************
 * @Function    : DMA_Config
 * @Description : DMA初始化配置
 * @Input       : None
 * @Output      : None
 * @Return      : None
 ***********************************************************/
static void DMA_Config(void)
{
	DMA_InitTypeDef DMA_InitStructure;
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);

	DMA_InitStructure.DMA_PeripheralBaseAddr = ADC1_DR_Address;             		
	DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t) AD_Value;       		
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;                      		
	DMA_InitStructure.DMA_BufferSize = (SAMP_COUNT + 1) * CIRCUIT;                                   			                 	
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;  
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable; 	
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord; 
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;         
	DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;                            
	DMA_InitStructure.DMA_Priority =  DMA_Priority_High;                     
	DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;

	DMA_Init(DMA1_Channel1, &DMA_InitStructure);                               
	DMA_ITConfig(DMA1_Channel1, DMA_IT_TC, ENABLE);
	DMA_Cmd(DMA1_Channel1,ENABLE);	                                           
}

/*******************************************************************************
 * @Function    : DMA_NVIC_Config
 * @Description : 配置NVIC
 * @Input       : None
 * @Output      : None
 * @Return      : None
 *******************************************************************************/
static void DMA_NVIC_Config(void)
{
    NVIC_InitTypeDef NVIC_InitStructure;
	
    NVIC_InitStructure.NVIC_IRQChannel = DMA1_Channel1_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}

/***********************************************************
 * @Function    : ADC1_Config
 * @Description : AD初始化配置
 * @Input       : None
 * @Output      : None
 * @Return      : None
 ***********************************************************/
static void ADC1_Config(void)
{
	ADC_InitTypeDef ADC_InitStructure;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1,ENABLE);
	RCC_ADCCLKConfig(RCC_PCLK2_Div6); 
	ADC_DeInit(ADC1);
	
	ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;
	ADC_InitStructure.ADC_ScanConvMode = ENABLE;
	ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;
	ADC_InitStructure.ADC_NbrOfChannel = CIRCUIT;
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
	ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_T2_CC2;

	ADC_Init(ADC1, &ADC_InitStructure);

	ADC_RegularChannelConfig(ADC1, ADC_Channel_10, 1, ADC_SampleTime_55Cycles5);      //PC0        
	ADC_RegularChannelConfig(ADC1, ADC_Channel_11, 2, ADC_SampleTime_55Cycles5);      //PC1

	ADC_DMACmd(ADC1, ENABLE);  	
	ADC_Cmd(ADC1, ENABLE);        

	ADC_ResetCalibration(ADC1);
	while(ADC_GetResetCalibrationStatus(ADC1));

	ADC_StartCalibration(ADC1);
	while(ADC_GetCalibrationStatus(ADC1));

//	ADC_SoftwareStartConvCmd(ADC1, ENABLE);
	ADC_ExternalTrigConvCmd(ADC1, ENABLE);
} 

/*******************************************************************************
 * @Function    : ADC1_Trans_Stop
 * @Description : 停止ADC转化
 * @Input       : None
 * @Output      : None
 * @Return      : None
 *******************************************************************************/
void ADC1_Trans_Stop(void)
{
    ADC_Cmd(ADC1, DISABLE); 
	DMA_Cmd(DMA1_Channel1,DISABLE);	   	
}

/*******************************************************************************
 * @Function    : ADC1_Restart
 * @Description : 重新开始ADC转换
 * @Input       : None
 * @Output      : None
 * @Return      : None
 *******************************************************************************/
void ADC1_Restart(void)
{
    ADC1_Config();
	DMA_Config();
	TIM2_Config();
}


/***********************************************************
 * @Function    : ADC1_Init
 * @Description : AD初始化
 * @Input       : None
 * @Output      : None
 * @Return      : None
 ***********************************************************/
void ADC1_Init(void)
{
	ADC1_GPIO_Config();
	DMA_NVIC_Config();
	DMA_Config();
	ADC1_Config();
	TIM2_Config();	
}

/*********************file end (c)MindX************************/
