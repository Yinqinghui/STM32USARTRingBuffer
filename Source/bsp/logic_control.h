/**
  ******************************************************************************
  * @file    logic_control.c
  * @author  cheng bb
  * @version V2.0
  * @date    2016-10-18
  * @brief   �߼�ִ��ͷ�ļ�
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2016 MindX</center></h2>
  ******************************************************************************
  */
#ifndef  __LOGIC_CONTROL_H_
#define  __LOGIC_CONTROL_H_

#include "stm32f10x.h"
#include "stm32f10x_it.h"

#define  POWER_RELAY_OPEN_COMMAND   "control_power relayE"
#define  TDS_RELAY_OPEN_COMMAND     "control_tds_relayE"
#define  LEAK_RELAY_OPEN_COMMAND    "control_leak_relayE"
#define  PWM_PUTOUT_COMMAND         "control_pwm_relayE" 
#define  ADC_THREE_FIVE_COMMEND     "control_adc_relayE"

extern uint8_t Flow_Flag;

void Test_Control(void);

#endif

/*********************file end (c)MindX************************/

