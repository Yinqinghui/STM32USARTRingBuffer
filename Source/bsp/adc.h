
/**
  ******************************************************************************
  * @file    adc.h
  * @author  cheng bb
  * @version V2.0
  * @date    2016-10-20
  * @brief   ADC配置头文件
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2016 MindX</center></h2>
  ******************************************************************************
  */
#include "stm32f10x.h"

#define ADC1_RCC              		RCC_APB2Periph_GPIOC
#define ADC1_RCC_AFIO               RCC_APB2Periph_AFIO
#define ADC1_GPIO_THREE_PIN         GPIO_Pin_1
#define ADC1_GPIO_FIVE_PIN          GPIO_Pin_0
#define ADC1_GPIO_PORT              GPIOC

#define CIRCUIT                     2                //多路采样
#define SAMP_COUNT                  10               //单周期采样次数

#define ADC1_DR_Address             ((uint32_t)0x40012400 + 0x4c)

extern  uint16_t AD_Value[SAMP_COUNT + 1][CIRCUIT];
extern uint8_t Flag_ADC;

void ADC1_Init(void);
void ADC1_Trans_Stop(void);
void ADC1_Restart(void);


/*********************file end (c)MindX************************/
